CREATE TABLE vaccins
(
    id_vaccin serial NOT NULL PRIMARY KEY,
    date timestamp NOT NULL,
    nom varchar(64) NOT NULL,
    id_user varchar(36) NOT NULL
);