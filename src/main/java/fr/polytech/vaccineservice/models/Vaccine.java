package fr.polytech.vaccineservice.models;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="vaccins")
@Access(AccessType.FIELD)
public class Vaccine {

    public Vaccine() {}
    
    public Vaccine(Date date, String nom, String id_user) {
        this.date = date;
        this.nom = nom;
        this.id_user = id_user;
    }

    public Vaccine(long id_vaccin, Date date, String nom, String id_user) {
        this.id_vaccin = id_vaccin;
        this.date = date;
        this.nom = nom;
        this.id_user = id_user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_vaccin;

    private Date date;
    private String nom;
    private String id_user;

    public long getId_vaccin() {
        return id_vaccin;
    }

    public void setId_vaccin(long id_vaccin) {
        this.id_vaccin = id_vaccin;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }
}
