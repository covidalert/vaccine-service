package fr.polytech.vaccineservice.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import fr.polytech.vaccineservice.models.Vaccine;
import fr.polytech.vaccineservice.services.VaccineService;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/vaccines")
public class VaccineController {
    @Autowired
    private VaccineService vaccineService;

    @Autowired
    private HttpServletRequest request;

    private String getUserIdFromToken() {

        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal=(KeycloakPrincipal)token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        AccessToken accessToken = session.getToken();
        return accessToken.getSubject().toString();
    }

    private boolean compareIdUserTokenAndIdUserVaccine(Long id) {
        String idUser = getUserIdFromToken();
        Optional<Vaccine> vaccine = vaccineService.get(id);
        return vaccine.isPresent() && vaccine.get().getId_user().equals(idUser);
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> get(@PathVariable Long id) {
        Optional<Vaccine> optionalVaccine = vaccineService.get(id);
        if(optionalVaccine.isPresent()) {
            return ResponseEntity.ok(optionalVaccine.get());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> post(@RequestBody final Vaccine vac) {
        try {
            Vaccine createdVaccine = vaccineService.create(vac);
            return ResponseEntity.created(new URI("/vaccines/"+createdVaccine.getId_vaccin())).build();
        } catch(URISyntaxException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping({"{id}"})
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        if(!compareIdUserTokenAndIdUserVaccine(id)){
            return new ResponseEntity<>("You cannot delete a vaccine that is not yours", HttpStatus.FORBIDDEN);
        }

        try {
            vaccineService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getVaccinesByUser(@PathVariable String id) {
        List<Vaccine> listVaccines = vaccineService.getVaccineByUser(id);
        return ResponseEntity.ok(listVaccines);
    }
}
