package fr.polytech.vaccineservice.repositories;

import fr.polytech.vaccineservice.models.Vaccine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VaccineRepository extends JpaRepository<Vaccine,Long> { }