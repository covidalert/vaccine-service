package fr.polytech.vaccineservice.services;

import fr.polytech.vaccineservice.models.Vaccine;
import fr.polytech.vaccineservice.repositories.VaccineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VaccineService {
    @Autowired
    private VaccineRepository vaccineRepository;

    public Optional<Vaccine> get(Long id) {
        return vaccineRepository.findById(id);
    }

    public Vaccine create(Vaccine vaccine) {
        return vaccineRepository.saveAndFlush(vaccine);
    }

    public List<Vaccine> getVaccineByUser(String id) {
        List<Vaccine> listUser = new ArrayList<Vaccine>();
        List<Vaccine> listFromBD = vaccineRepository.findAll();
        for(Vaccine vac : listFromBD){
            if((vac.getId_user()).equals(id)){
                listUser.add(vac);
            }
        }

        return listUser;
    }

    public void delete(Long id) throws NoSuchElementException {
        if(vaccineRepository.existsById(id)){
            vaccineRepository.deleteById(id);
        } else {
            throw new NoSuchElementException();
        }
    }
}
