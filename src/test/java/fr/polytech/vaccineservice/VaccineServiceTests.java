package fr.polytech.vaccineservice;

import fr.polytech.vaccineservice.models.Vaccine;
import fr.polytech.vaccineservice.repositories.VaccineRepository;
import fr.polytech.vaccineservice.services.VaccineService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.util.Optional;

import static org.mockito.Mockito.*;

@WebMvcTest(VaccineService.class)
public class VaccineServiceTests {
    @Autowired
    private VaccineService vaccineService;

    @MockBean
    private VaccineRepository repository;

    @Test
    @DisplayName("get - Success")
    public void getSuccess() {
        Vaccine mockVaccine = new Vaccine(1,new Timestamp(1000),"Pfizer", "aaaaaaa");
        doReturn(Optional.of(mockVaccine)).when(repository).findById(1L);

        Optional<Vaccine> foundTest = vaccineService.get(1L);

        Assertions.assertTrue(foundTest.isPresent(), "Vaccine was not found");
        Assertions.assertSame(foundTest.get(), mockVaccine, "Vaccines should be the same");
    }

    @Test
    @DisplayName("get - Not found")
    public void getNotFound() {
        doReturn(Optional.empty()).when(repository).findById(1L);

        Optional<Vaccine> foundTest = vaccineService.get(1L);

        Assertions.assertFalse(foundTest.isPresent(), "Vaccine was found when it should not be");
    }

    @Test
    @DisplayName("create - Success")
    public void createSuccess() {
        Vaccine mockVaccine = new Vaccine(1,new Timestamp(1000),"Pfizer", "aaaaaaa");
        doReturn(mockVaccine).when(repository).saveAndFlush(mockVaccine);

        Vaccine createdTest = vaccineService.create(mockVaccine);

        Assertions.assertSame(createdTest, mockVaccine, "Incoming Vaccine and created Vaccine should be the same");
    }
}
