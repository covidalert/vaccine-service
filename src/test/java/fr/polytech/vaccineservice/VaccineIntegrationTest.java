package fr.polytech.vaccineservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.polytech.vaccineservice.models.Vaccine;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Transactional
@AutoConfigureTestEntityManager
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class VaccineIntegrationTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MockMvc mockMvc;

    private String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("GET /vaccines/{id} - Found")
    public void getFound() throws Exception {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000000), "Pfizer", "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        mockMvc.perform(get("/vaccines/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id_vaccin", is(id.intValue())))
                .andExpect(jsonPath("$.nom", is("Pfizer")))
                .andExpect(jsonPath("$.id_user", is("a34kAo90")));
    }

    @Test
    @DisplayName("GET /vaccines/{id} - Not Found")
    public void getNotFound() throws Exception {
        // Add then remove the entry in the db, to ensure that the id is unavailable
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000000), "Pfizer", "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        entityManager.remove(mockVaccine);

        mockMvc.perform(get("/vaccines/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /vaccines - Success")
    public void postSuccess() throws Exception {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000000), "Pfizer", "a34kAo90");

        mockMvc.perform(post("/vaccines")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(mockVaccine)))
                .andExpect(status().isCreated());
    }
}
