package fr.polytech.vaccineservice;

import fr.polytech.vaccineservice.controllers.VaccineController;
import fr.polytech.vaccineservice.models.Vaccine;
import fr.polytech.vaccineservice.services.VaccineService;
import org.junit.Before;
import org.junit.jupiter.api.Test;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Optional;


import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VaccineController.class)
@ComponentScan(basePackageClasses = { KeycloakSecurityComponents.class, KeycloakSpringBootConfigResolver.class })
class VaccineControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private VaccineService vaccineService;

	private String asJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Test
	@DisplayName("GET /vaccines/1 - Found")
	@WithMockUser
	public void shouldReturnTestEntity() throws Exception {
		Vaccine mockVaccine = new Vaccine(1, new Timestamp(1000000), "Pfizer", "a34kAo90");
		doReturn(Optional.of(mockVaccine)).when(vaccineService).get(1L);
		mockMvc.perform(get("/vaccines/1", 1))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id_vaccin", is(1)))
				.andExpect(jsonPath("$.nom", is("Pfizer")))
				.andExpect(jsonPath("$.id_user", is("a34kAo90")));

	}


	@Test
	@DisplayName("GET /vaccines/1 - Not Found")
	public void shouldNotReturnTestEntity() throws Exception {
		doReturn(Optional.empty()).when(vaccineService).get(1L);

		mockMvc.perform(get("/vaccines/1", 1))
				.andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("POST /vaccines - Success")
	public void shouldCreateEntity() throws Exception {
		Vaccine postVaccine = new Vaccine(new Timestamp(1000000), "Pfizer", "a34kAo90");
		Vaccine mockVaccine = new Vaccine(1, new Timestamp(1000000), "Pfizer", "a34kAo90");
		doReturn(mockVaccine).when(vaccineService).create(any());

		mockMvc.perform(post("/vaccines")
						.contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(postVaccine)))
				// Response type and content type
				.andExpect(status().isCreated())

				// Validate the headers
				.andExpect(header().string(HttpHeaders.LOCATION, "/vaccines/1"));

				/* Uncomment if the entity is returned with a post method
				// Returned fields
				.andExpect(jsonPath("$.id_test", is(1)))
				.andExpect(jsonPath("$.nom", is("Pfizer")))
				.andExpect(jsonPath("$.is_negative", is(true)))
				.andExpect(jsonPath("$.id_user", is("ieu6azZA")));
				*/
	}
/* If delete method is done
	@Test
	@DisplayName("DELETE /vaccines/1 - Success")
	public void shouldDeleteEntity() throws Exception {
		Vaccine mockVaccine = new Vaccine(1, new Timestamp(1000000), "Pfizer", "a34kAo90");
		doNothing().when(vaccineService).delete(1L);

		mockMvc.perform(delete("/tests/1", 1))
				.andExpect(status().isOk());
	}

	@Test
	@DisplayName("DELETE vaccines/1 - Not found")
	public void shouldNotFindEntityOnDelete() throws Exception {
		doThrow(NoSuchElementException.class).when(vaccineService).delete(any());
		mockMvc.perform(delete("/tests/{id}", 1))
				.andExpect(status().isNotFound());
	}
*/
}
