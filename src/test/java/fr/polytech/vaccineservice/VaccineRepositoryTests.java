package fr.polytech.vaccineservice;

import fr.polytech.vaccineservice.models.Vaccine;
import fr.polytech.vaccineservice.repositories.VaccineRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class VaccineRepositoryTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private VaccineRepository repository;

    @Test
    @DisplayName("FindById - Found")
    void testFindByIdFound() {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        Optional<Vaccine> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertTrue(foundTest.isPresent(), "Vaccine was not found");
        Assertions.assertSame(foundTest.get(), mockVaccine,  "Vaccine found was not the same");
    }

    @Test
    @DisplayName("FindById - Not found")
    void testFindByIdNotFound() {
        // Data persists through every tests, so to ensure that a data with the provided id is not found,
        // we look for a data that has just been removed
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        entityManager.remove(mockVaccine);

        Optional<Vaccine> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertFalse(foundTest.isPresent(), "Vaccine was found");
    }

    @Test
    @DisplayName("SaveAndFlush - Success")
    public void testSaveAndFlushSuccess() {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Vaccine createdTest = repository.saveAndFlush(mockVaccine);

        // Assertions
        Assertions.assertSame(createdTest, mockVaccine,"Vaccine was not the same");
        entityManager.remove(mockVaccine);
    }

    @Test
    @DisplayName("ExistsById - Found")
    public void existsByIdFound() {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertTrue(isFound, "Vaccine was not found");
    }

    @Test
    @DisplayName("ExistsById - Not found")
    public void existsByIdNotFound() {
        // Same than FindById not found
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        entityManager.remove(mockVaccine);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertFalse(isFound, "Vaccine was not found");
    }

    @Test
    @DisplayName("ExistsById - Provided id null")
    public void existsByIdThrowException() {
        // Assertions
        Assertions.assertThrowsExactly(InvalidDataAccessApiUsageException.class, () -> repository.existsById(null));
    }

    @Test
    @DisplayName("DeleteById - Success")
    public void deleteByIdSuccess() {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        repository.deleteById(id);
    }

    @Test
    @DisplayName("DeleteById - Not found")
    public void deleteByIdNotFound() {
        Vaccine mockVaccine = new Vaccine(new Timestamp(1000),"Pfizer", "azert");
        Long id = (Long) entityManager.persistAndGetId(mockVaccine);
        entityManager.remove(mockVaccine);

        // Assertions
        Assertions.assertThrowsExactly(EmptyResultDataAccessException.class, () -> repository.deleteById(id));
    }
}
